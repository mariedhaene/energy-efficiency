\documentclass[a4paper]{kulakarticle}

\usepackage[dutch]{babel}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath, amssymb}
\usepackage[separate-uncertainty=true]{siunitx}
\usepackage{subcaption}
\usepackage{float}
\usepackage{booktabs}
\usepackage{parskip}
\usepackage[labelfont=bf]{caption}
\usepackage{systeme}
\usepackage{cmbright}
\usepackage[left=1.2in,right=0.8in,top=1.5in,bottom=1in]{geometry}
\usepackage{xparse}
\usepackage{transparent}
\usepackage{mathtools}
\usepackage{cprotect}
\usepackage[space]{grffile}
\usepackage{pdfpages}
\usepackage{gensymb}
%\pdfinclusioncopyfonts=1
% \usepackage{titlesec}

% figure support
\usepackage{import}
\usepackage{xifthen}
\usepackage{pdfpages}
\usepackage{transparent}
\newcommand{\incfig}[2][1]{%
	\def\svgwidth{#1\columnwidth}
	\import{./Figuren/}{#2.pdf_tex}
} %

\graphicspath{{./Figuren/}
,{/Users/Hoofdgebruiker/Documents/Kulak/fase 3/sem 2/smda/smda-project-2/Verslag/Figuren/}
}
% \titlespacing\subsection{0.2pt}{1.0ex plus 1ex minus -1ex}{-\parskip}
% \titlespacing\section{0.2pt}{2ex plus -0.5ex minus -.2ex}{-\parskip}

\newcommand{\tabitem}{~~\llap{\textbullet}~~}

\address{X0C87B \\
Statistische modellen en data-analyse \\
Jakob Raymaekers en Stijn Rebry}
\title{Regressiemodel voor energie-effici\"entie van woningen}
\author{Marie D'haene (r0710108) en Judith Vandewiere (r0715595)}
\date{Academiejaar 2019--2020}

\begin{document}
\maketitle
\vspace{1em}
\section*{Inleiding}
In dit verslag gaan we met behulp van gegevens verkregen door een aantal woningsimulaties op zoek naar een regressiemodel dat de energie-efficiëntie van een woning zo goed mogelijk verklaart.
De simulaties werden gemaakt met \verb|Ecotect| door ir.\ Angeliki Xifari.
Tabel \ref{tab:variabelen} lijst de verschillende variabelen van de dataset op.
De eerste acht zijn de predictorvariabelen.
De respons is de totale energie nodig om de temperatuur van de woning constant te houden: \verb|Energy Load = Heating Load + Cooling Load|.
Tijdens de simulaties werden luchtvochtigheid, ligging (namelijk Athene) en het volume $V$ van de woning (namelijk $\SI{771.75}{\metre\cubed}$) constant gehouden.
De gesimuleerde woningen werden immers opgebouwd uit 18 kubussen met een zijde van $\SI{3.5}{\metre}$.
Door deze manier van werken, hebben de huizen in deze simulatie een hoogte van ofwel $\SI{3.5}{\metre}$ (\'e\'en kubus hoog) of $\SI{7}{\metre}$ (twee kubussen hoog).
% \textcolor{red}{deze zin is overbodig in de inleiding zeker? $\rightarrow$}Toch kiezen we er niet voor om van de variabele \verb|Overall Height| een categorische veranderlijke te maken, omdat ons finale model dan nog steeds werkt voor simulaties waarbij men gebruik zou maken van andere blokjes, of woningen met meerdere verdiepingen.

We beginnen ons onderzoek met een analyse van de predictoren: hun correlatie en de relevantie van mogelijks categorische veranderlijken voor de energie-efficiëntie.
Daarna bepalen we in het kader van ordinary-least-squares-regressie (OLS), ons baserend op stapsgewijze en all-subsets algoritmes, een werkbaar regressiemodel. Hiermee bedoelen we een model dat een behoorlijk stuk van de total sum of squares (SST) verklaart, maar ook geen te hoge variance-inflating factors (VIF's) heeft.
Daarna komt Ridgeregressie aan bod.
Ook voeren we least-trimmed-squares-regressie (LTS) uit om te onderzoeken of er uitschieters zijn.
% In het laatste onderdeel bespreken we het finale regressiemodel en geven een interpretatie van de co\"effici\"enten.

Alle resultaten en grafieken werden bekomen door gebruik van \verb|R| en doorheen heel het verslag wordt een significantieniveau van 5\% gebruikt.

\begin{table}[h]
	\centering
	\begin{tabular}{||r|l||}
		\hline
		\textbf{Naam} & \textbf{Omschrijving} \\
		\hline
		 & \\[-0.8em]
		\verb|Relative Compactness| & Maat voor compactheid van de woning:\\[-0.1em]
		& $\texttt{Relative Compactness} = 6 \cdot V^{0.66} \cdot \left(\texttt{Surface Area}\right)^{-1}$ \\[0.15em]
		\verb|Surface Area| & Totale oppervlakte van dak, buitenmuren en grondvlak van de\\[-0.1em]
		& woning (in m$^2$) \\[0.15em]
		\verb|Wall Area| & Totale oppervlakte van buitenmuren van de woning (in m$^2$) \\[0.15em]
		\verb|Roof Area| & Dakoppervlakte van de woning (in m$^2$) \\[0.15em]
		\verb|Overall Height| & Hoogte van de woning in m \\[-0.1em]
		& (in dit geval enkel woningen van $\SI{3.5}{\metre}$ of $\SI{7}{\metre}$) \\[0.15em]
		\verb|Orientation| &   Oriëntatie voorkant van de woning: noord (2), oost (3), zuid (4) en \\[-0.1em]
		& west (5) \\[0.15em]
		\verb|Glazing Area| & Oppervlakte van de woning die uit glas bestaat (in percentage van \\[-0.1em]
		& \verb|Roof Area|) \\[0.15em]
		\verb|Glazing Area Distribution| & Verdeling van de glasoppervlakte van de woning \\
		&\tabitem 0: woning zonder ramen (glasoppervlakte is 0\%) \\
		&\tabitem 1: 25\% aan elke zijde\\
		&\tabitem 2: 55\% aan noordkant en 15\% aan andere zijden\\
		&\tabitem 3: 55\% aan oostkant en 15\% aan andere zijden\\
		&\tabitem 4: 55\% aan zuidkant en 15\% aan andere zijden\\
		&\tabitem 5: 55\% aan westkant en 15\% aan andere zijden\\[0.15em]
		\verb|Heating Load| & Hoeveelheid energie (in kWh) die per jaar per m$^2$ grondoppervlakte \\[-0.1em]
		& toegevoegd moet worden om de temperatuur tussen 19$\degree$C en 24$\degree$C \\[-0.1em]
		& te houden\\[0.15em]
		\verb|Cooling Load| & Hoeveelheid energie (in kWh) die per jaar per m$^2$ grondoppervlakte \\[-0.1em]
		& verwijderd moet worden om de temperatuur tussen 19$\degree$C en 24$\degree$C \\[-0.1em]
		&te houden\\[0.5em]
		\hline
	\end{tabular}
	\cprotect\caption{Variabelen in de dataset \verb|energyEfficiency|.
	De eerste acht worden gebruikt als predictoren om het totale energieverbruik (\verb|Heating Load + Cooling Load|) van een woning te verklaren.
	Het volume $V$ van elke woning is hetzelfde, namelijk $\SI{771.75}{\metre\cubed}$.}
	\label{tab:variabelen}
\end{table}

\section{Predictoronderzoek}
\label{sec:pred}
Vooraleer regressiemodellen voor \verb|Energy Load| te bestuderen, maken we een korte analyse van de kandidaatpredictoren.
We bekijken de correlatiematrix en gaan na of het de moeite loont categorische predictoren in het model op te nemen.

\subsection{Correlaties}
Zoals te verwachten, treedt er enerzijds een hoge correlatie op tussen de variabelen die te maken hebben met de afmetingen van het huis en anderzijds ook een niet-verwaarloosbare correlatie tussen variabelen die iets zeggen over de beglazing.
Een overzicht van de belangrijkste correlaties is gegeven in Figuur \ref{fig:correlaties}.

We verwachten dus zeer sterke multicollineariteit indien we meerdere afmeting-gerelateerde parameters gebruiken om een regressiemodel op te stellen.
Hier letten we dan ook op bij het kiezen van een model in Sectie \ref{sec:ols}.
Bovendien wordt \verb|Relative Compactness| bepaald als $6 \cdot V^{0.66} \cdot (\texttt{Surface Area})^{-1}$. Er bestaat met andere woorden een perfect omgekeerd evenredig verband tussen \verb|Relative Compactness| en \verb|Surface Area|.
Daarom besluiten we om van in het begin \verb|Surface Area| niet meer als mogelijke predictor te beschouwen.
We kiezen voor de totale oppervlakte eerder dan voor de compactheid omdat er tevens geldt dat $\verb|Surface Area| = 2\cdot\verb|Roof Area| + \verb|Wall Area|$ waardoor alle informatie over de oppervlakte reeds vervat zit in de wand- en dakoppervlakte.
Er wordt in wat volgt dus geen rekening meer gehouden met deze veranderlijke.

Ondanks het feit dat de correlatie tussen de beglazingsparameters niet overdreven groot is, zullen we toch enkel rekening houden met \verb|Glazing Area|.
Dit wordt hieronder gemotiveerd.

\begin{figure}[ht]
    \centering
    \incfig{predictoren}
    \cprotect\caption{Schematisch overzicht van de belangrijkste correlaties tussen de predictoren.
		De eerste kolom bestaat uit veranderlijken die te maken hebben met de afmetingen van het huis.
		De correlatie van \verb|Wall Area| met de andere predictoren in de kolom varieert tussen $0.2$ en $0.3$.
		De tweede kolom bestaat uit veranderlijken die te maken hebben met de beglazing en de derde kolom bevat de ori\"entatie van het huis, die nergens mee gecorreleerd is, zoals te verwachten.}
    \label{fig:correlaties}
\end{figure}

\subsection{Mogelijks categorische variabelen}
De veranderlijken \verb|Orientation|, \verb|Overall Height| en \verb|Glazing Area (Distribution)| bestaan uit discrete niveaus en zouden dus als categorische voorspellers meegenomen kunnen worden in een model.
Om te bepalen of dit al dan niet relevant is, bestuderen we ze een voor een aan de hand van eenvoudige lineaire regressiemodellen waarbij we de veranderlijke telkens numeriek en categorisch beschouwen.
De boxplots in Figuur \ref{fig:boxplot} geven reeds aan dat we geen significante bijdrage verwachten van \verb|Orientation| en \verb|Glazing Area Distribution|, maar wel van \verb|Overall Height|.

Onze vermoedens voor \verb|Glazing Area Distribution| en \verb|Orientation| worden bevestigd door de Tukey HSD-test: ori\"entatie van de woning heeft geen invloed op de energie-efficiëntie ervan (p-waarde $> 99\%$) en het maakt ook niet uit waar de ramen zitten (p-waarde~$> 99\%$).
Wel is het relevant of er al dan niet ramen aanwezig zijn in het huis (p-waarde $< 0.06\%$). We zouden dus een nieuwe veranderlijke kunnen invoeren die weergeeft of er wel of geen ramen aanwezig zijn, maar deze informatie zit eigenlijk al vervat in \verb|Glazing Area| (zie ook Figuur \ref{fig:boxplot}).
Een beglazingsoppervlakte die gelijk is aan nul betekent dat er geen ramen aanwezig zijn. Bovendien leert de Tukey HSD-test ons dat er een significant verschil (p-waarde $< 2\%$) is tussen de energie-efficiëntie van woningen op basis van hoeveel procent ramen ze bevatten.
We besluiten daarom om zowel \verb|Glazing Area Distribution| en \verb|Orientation| niet meer als relevante predictoren te beschouwen in wat volgt.

Daarnaast merken we dat \verb|Overall Height| een significante invloed heeft op \verb|Energy Load|; de p-waarde van de Tukey HSD-test (wanneer we de predictor als categorisch beschouwen) is nagenoeg nul.
Omdat er echter maar twee niveaus zijn in \verb|Overall Height|, namelijk $\SI{3.5}{\metre}$ of $\SI{7}{\metre}$, komt het model met de hoogte als continue regressor op hetzelfde neer als dat met de hoogte als categorische regressor.
De regressierechte gaat immers door het gemiddelde van de twee groepen, zoals duidelijk in Figuur \ref{fig:overall_height}, en bijgevolg verklaren beide modellen evenveel van de SST.
Daarom kiezen we ervoor in het vervolg de hoogte als continue variabele te blijven beschouwen.
Dit levert immers ook de mogelijkheid om voorspellingen te doen voor huizen die een andere hoogte hebben dan $\SI{3.5}{\metre}$ of $\SI{7}{\metre}$.

\begin{figure}
	\centering
	\begin{subfigure}{0.45\textwidth}
		\includegraphics[width=\textwidth]{boxplot_height}
	\end{subfigure}%
	\begin{subfigure}{0.45\textwidth}
		\includegraphics[width=\textwidth]{boxplot_orientation}
	\end{subfigure}

	\begin{subfigure}{0.45\textwidth}
		\includegraphics[width=\textwidth]{boxplot_gad}
	\end{subfigure}%
	\begin{subfigure}{0.45\textwidth}
		\includegraphics[width=\textwidth]{boxplot_ga}
	\end{subfigure}

	\cprotect\caption{Boxplots van mogelijks categorische predictoren met de totale gemiddelde \verb|Energy Load| in het rood en het gemiddelde per groep in het blauw.
	Deze grafieken doen ons verwachten dat er een significant verschil is in de gemiddelde energie-efficiëntie tussen de groepen bepaald door de hoogte en tussen de groepen bepaald door de beglazing.
	Hier is er echter enkel onderscheid tussen huizen met ramen en huizen zonder ramen.}
	\label{fig:boxplot}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{overall_height}
	\cprotect\caption{Regressiemodel voor \verb|Energy Load| met \verb|Overall Height| als categorische (rode balken) en continue (blauwe lijn) voorspeller.
	De regressierechte gaat perfect door beide groepsgemiddeldes aangezien er slechts twee groepen zijn, bijgevolg verklaren beide modellen even veel van de SST.}
	\label{fig:overall_height}
\end{figure}

\section{Regressie}
\label{sec:ols}
% \textcolor{red}{($*$) moet het stuk dat we in de code in het begin van "wat zijn de belangrijkste predictoren ook nog hier besproken worden?}
Op basis van de resultaten besproken in Sectie \ref{sec:pred} zullen we variabelen \verb|Surface Area|, \verb|Glazing Area Distribution| en \verb|Orientation| achterwege laten. We bekijken wat nu het `beste' model is dat we kunnen verkrijgen met zo weinig mogelijk multicollineariteit en of eventuele interactietermen met \verb|Overall Height| nuttig zijn.
\subsection{Klassieke regressie}
Wanneer we stapsgewijze regressie uitvoeren (voorwaarts, achterwaarts en beide richtingen), dan bekomen we drie keer hetzelfde model.
Bij voorwaartse regressie wordt eerst \verb|Overall Height| toegevoegd, daarna achtereenvolgens \verb|Glazing Area|, \verb|Wall Area|, \verb|Relative Compactness| en \verb|Roof Area|.
% \textcolor{red}{als ($*$) dan kunnen we nog eens tonen dat Orientation inderdaad weggelaten wordt, en Glazing Area Distribution}

Ook uit de all-subsets-methode leiden we hetzelfde af: vooral \verb|Overal Height| en \verb|Glazing Area| komen het vaakst voor als regressoren.
Modellen met vijf variabelen scoren het best (lage BIC), maar zoals in Figuur \ref{fig:regsubset} te zien is, zijn modellen met vier en zelfs drie predictoren niet veel slechter.
Bovendien hebben zij het voordeel dat ze eenvoudiger zijn om mee te werken en ook minder multicollineariteit vertonen, want de variabelen in \verb|energyEfficiency| zijn sterk gecorreleerd.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{regsubset}
	\cprotect\caption{Vergelijking van BIC voor modellen met een verschillend aantal predictoren.
	Kleinste BIC wordt bereikt bij een model met vijf predictoren ($p=6$), maar modellen met vier of drie ($p= 5$, $p=4$) predictoren presteren niet veel slechter.}
	\label{fig:regsubset}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{vifplot}
	\cprotect\caption{Vergelijking van de VIF's voor de modellen met drie~(groen), vier~(rood) en vijf~(blauw) predictoren zoals besproken in Sectie~\ref{sec:ols}.
	%De nummers komen overeen met de volgorde van predictoren toevoegen aan het model.
	Het nummer van de predictor komt overeen met de volgorde waarin die wordt toegevoegd aan het model (dus eerst \verb|Overall Height|, dan \verb|Glazing Area| enzovoort).
	De gemiddelde VIF per model staat in het corresponderende kleur weergegeven met een horizontale lijn.
	Punten boven de gestreepte lijn, hebben een VIF groter dan 10, wat op sterke multicollineariteit wijst.
	Indien het gemiddelde van de VIF's veel groter is dan 1 (dus ver boven de gestipte lijn op hoogte nul), wijst dit ook op sterke multicollineariteit.
	Deze grafiek bevestigt dat het model met drie predictoren aangewezen is om de energie-efficiëntie van een woning te verklaren.}
	\label{fig:vif}
\end{figure}

Om een werkbaar model te zoeken op basis van voorgaande observaties, vergelijken we nu volgende modellen met drie, vier en vijf predictoren:
\begin{align}
	\texttt{Energy Load} =& -40.08 + 9.34 \texttt{ Overall Height} + 35.26 \texttt{ Glazing Area} + 0.09 \texttt{ Wall Area}
	\label{eq:model3} \\[0.5em]
	\texttt{Energy Load} =& -11.59 + 11.34 \texttt{ Overall Height} + 35.26 \texttt{ Glazing Area} + 0.05 \texttt{ Wall Area} \nonumber \\
	& -34.53 \texttt{ Relative Compacteness}
	\label{eq:model4} \\[0.5em]
	\texttt{Energy Load} =& \: 182.15 + 8.45 \texttt{ Overall Height} + 35.26 \texttt{ Glazing Area} + -0.07 \texttt{ Wall Area} \nonumber \\
	& - 135.56 \texttt{ Relative Compacteness} - 0.035 \texttt{ Roof Area}.
	\label{eq:model5}
\end{align}
We weten dankzij stepwise regression reeds dat al deze modellen een groot deel van de SST verklaren ($R^2_{\text{adj}} > 0.9$).
Bij geen enkel model is echter aan de modelveronderstellingen voldaan, waardoor het enige criterium om te vergelijken de variance-inflating factors (VIF's) zijn.
Deze staan weergegeven in Figuur~\ref{fig:vif}.
Het is duidelijk dat er multicollineariteit optreedt zodra we \verb|Relative Compactness| toevoegen bij model~\eqref{eq:model4}: het gemiddelde van de VIF's is groter dan 1 en er zijn twee VIF's die nagenoeg 10 zijn.
Dit is wegens de hoge correlatie tussen \verb|Overall Height| en \verb|Relative Compactness|~(0.83).
In het model met vijf predictoren wordt de multicollineariteit nog sterker, waardoor de co\"effici\"ent bij \verb|Wall Area| zelfs van teken verandert (terwijl we hier wel degelijk een plusteken verwachten).
Enkel de VIF die hoort bij \verb|Glazing Area| blijft constant doorheen alle modellen, omdat die niet gecorreleerd is met de andere variabelen die we hier hanteren.
Zowel bij modellen met drie, vier als vijf predictoren voegt een interactieterm met \verb|Overall Height| weinig toe.
Ook levert het zelfs hogere VIF's op.
We concluderen dus dat het model met drie predictoren \eqref{eq:model3} de energie-efficiëntie van een woning het best kan verklaren, ook al is hier niet aan de modelveronderstellingen voldaan (zie Figuur \ref{fig:modelverond}).
Transformaties of gewogen regressie bieden hier geen oplossing voor.
Ook principaalcomponentregressie levert geen verbetering.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{modelverond}
	\caption{Diagnostische grafieken voor het model uit vergelijking \eqref{eq:model3}.
	De twee groepen zichtbaar bij de residuen zijn afkomstig van huizen met verschillende hoogte.
	De p-waarde bij de Shapiro-Wilktest is~$2.92 \cdot 10^{-13}$, de residuen zijn dus niet normaal verdeeld.
	}
	\label{fig:modelverond}
\end{figure}

In model \eqref{eq:model3} zien we de verbanden die we verwachten: hoe hoger het huis, hoe meer energie nodig om de temperatuur op peil te houden.
Daarnaast heeft een huis met meer ramen ook meer energie nodig, zoals we verwachten, want er gaat meer warmte verloren via glas dan via een muur.
Tot slot zorgt een grotere muuroppervlakte ook voor een lagere energie-effici\"entie (grotere \verb|Energy Load|).

\subsection{Ridgeregressie}
Aangezien er veel correlatie is tussen de vijf variabelen die we beschouwen om een regressiemodel mee op te bouwen, voeren we Ridgeregressie uit.
Het resulterende model is
\begin{align}
	\texttt{Energy Load} =& \: 82.72 - 78.79 \texttt{ Relative Compactness} - 0.08  \texttt{ Wall Area} - 0.37 \texttt{ Roof Area} \nonumber \\
	& + 8.10 \texttt{ Overall Height} + 34.13 \texttt{ Glazing Area}
\end{align}
waarbij we een trainings- en validatieset gebruiken om de optimale waarde voor de verschuivingsparameter (die is $1$ in dit geval) te vinden.
We zien echter dat \verb|Wall Area| en \verb|Roof Area| een verkeerd teken hebben, want we verwachten dat een grotere dak- of muuroppervlakte aanleiding geeft tot een lagere energie-efficiëntie.
Bijgevolg gebruiken we dit model niet.

\subsection{Outliers}
Aan de hand van least-trimmed-squares-regressie kan nagegaan worden of de dataset invloedrijke datapunten bezit.
We zijn hier kort op ingegaan en concluderen dat er geen outliers zijn in \verb|energyEfficiency| op basis van de robuuste Mahalanobisafstand versus de LTS-residuen van het model met dezelfde predictoren als in~\eqref{eq:model3}.
Daarnaast komen de studentized residuals zeer goed overeen met de residuen uit model~\eqref{eq:model3}.
Deze twee observaties blijken duidelijk uit Figuur~\ref{fig:outliers}.
Ook op de diagnostische grafieken in Figuur~\ref{fig:modelverond} zijn niet onmiddellijk uitschieters te zien.
Tot slot merken we nog kort op dat we in dit geval ook geen outliers verwachten aangezien de data verkregen is met behulp van een simulatie, wat een `gecontroleerde omgeving' is: extreme waarden door uitzonderlijke weersomstandigheden, defecten in materialen of meetfouten kunnen hier niet voorkomen.

\begin{figure}
	\centering
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{rdiag_lts}
	\end{subfigure}%
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{rstudent}
	\end{subfigure}
	\caption{(links) Robuuste afstand en residuen verkregen door least-trimmed-squares-regressie met dezelfde predictoren als in model \eqref{eq:model3}.
	(rechts) Studentized residuen in functie van de gestandaardiseerde residuen van model \eqref{eq:model3}.
	Beide grafieken geven aan dat er geen invloedrijke datapunten zijn in deze dataset.}
	\label{fig:outliers}
\end{figure}

\section{Conclusie}
Het model dat we gebruiken om de energie-efficiëntie van een woning te verklaren is
\begin{equation*}
	\texttt{Energy Load} = -40.08 + 9.34 \texttt{ Overall Height} + 35.26 \texttt{ Glazing Area} + 0.09 \texttt{ Wall Area}.
	\label{eq:finaalmodel}
\end{equation*}
Om te beginnen, elimineerden we \verb|Surface Area| omdat deze veranderlijke in lineair verband staat met (\verb|Relative Compactness|)$^{-1}$.
Uit verder onderzoek van \verb|Glazing Area Distribution| en \verb|Orientation| (twee variabelen die we als categorisch kunnen beschouwen) bleek dat deze geen significante invloed hebben op \verb|Energy Load| en deze werden dan ook achterwege gelaten.
Uit stepwise regression en all-subsets regression bleek dat een `optimaal' model vijf predictoren bevat, maar dat modellen met drie of vier predictoren het niet veel slechter doen.
Aan de hand van variance-inflating factors bepaalden we tot slot welk model het `beste' was.
De modelveronderstellingen zijn echter nog steeds niet voldaan, maar transformaties of herweging van de residuen boden geen oplossing.
Ook Ridgeregressie leverde geen verbetering op het model dat we reeds hadden, dus gebruiken we bovenstaand model.
Het is een eenvoudig en werkbaar model en alle co\"effici\"enten hebben het teken dat we verwachten.
Ten slotte bleek uit een onderzoek naar outliers dat deze niet aanwezig zijn in de dataset, zoals we verwachten van een simulatie.
Het zou dan ook nuttig zijn het bekomen model te testen aan de hand van gegevens van `echte' woningen.

\end{document}
