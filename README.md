*Deadline vrijdag 19 juni*

We zouden best Surface Area of Relative Compactness al van in het begin weglaten want *perfect* lineair verband.
Dit lijkt mij super slecht.

# Hoe gaan we dit in godsnaam aanpakken?
Misschien kijken per categorische predictor of gemiddelde Energy.Load verschilt tussen de groepen om al een idee te hebben of de predictor relevant zal zijn voor het model of niet.

## Least squares schattingen
- Stapsgewijze en all-subsets regressie uitvoeren en vergelijken.
- Invloed van categorische variabelen bekijken zoals in de oefenzittingen.

## Least trimmed squares schattingen
- Dit doen en vergelijken met resultaten van LS schattingen om te zien of er outliers zijn.
- Ook nog om te zien of er outliers zijn: diagnostic plot maken.

## Multicollineariteit checken
- Door te kijken naar de correlaties.
- ...
- Eventueel PCR uitvoeren, want dit lijkt me handig om het aantal predictoren te verkleinen.
